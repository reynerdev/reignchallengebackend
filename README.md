# Reign Backend Challenge

First, the projects start by launching our docker-compose file with the following command:
```bash
docker docker-compose up --build
```
After that operation, the MongoDB image will be downloaded from the docker hub   and the docker file will be built as well in order to generate the image to start the node server container.

Then we will have the three containers running and sharing ports.

1-Container : MongoDB 
2-Container: Node js Server with an initial command npm run dev , expose port 3000 . This will create a database called *database* with a collection articles.   
3-Container: Node js Server with a initial command npm run  dev_test, expose port 3001 (NODE_ENV=test). This container will create a database called *database_test" with no documents in the collection of articles. This container helps  us to test our project

**We are using docker volumes in order to persist the data after we shut down the container.**

**We point a reference volumen in the client from the container in the following location ~/database in order to persist data after a restart**

In order to populate the data we need to make a get request to the following service

```javascript
http://localhost:3000/populate
```
A Postman file is also given , you can import it in the app and make use of the preconfigured requests.**ReignPostman.postman_collection.json**

Another way to access the api docs , it's with the following endpoint

```javascript
http://localhost:3000/swagger
```
