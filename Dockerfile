# base image
FROM node:16-alpine
WORKDIR /usr/app

# install dependencies
RUN npm install -g nodemon
COPY ./package.json .
RUN npm install

# copy files
COPY  . .

# CMD [ "nodemon" , ".","index.js"]
CMD [ "npm" ,"run","dev"]
