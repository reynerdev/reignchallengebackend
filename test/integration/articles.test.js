const request = require('supertest');
const Article = require('../../src/model/Article');

let server;

describe('Articles', () => {
  beforeEach(() => {
    server = require('../../index');
  });
  afterEach(async () => {
    server.close();
    await Article.remove({});
  });

  describe('GET /', () => {
    // due to pagination, only 5 artciles per page maximum
    it('should return 5 articles at maximum', async () => {
      // populate the data base
      await request(server).get('/populate');
      const res = await request(server).get('/');

      expect(res.body.result.length).toBeLessThanOrEqual(5);
    });

    // remove all articles in the collection
  });

  describe('GET /:id', () => {
    it('should return a article if a valid id is passed as param', async () => {
      const article = new Article({ title: 'Node JS conference', author: 'Reyner Loza', _tags: ['nodejs', 'express'] });
      await article.save();
      const res = await request(server).get(`/${article._id}`);

      expect(res.status).toBe(200);
      expect(res.body.article).toHaveProperty('title', article.title);
    });
    it('should return 404 if a invalid id is passed as param', async () => {
      const res = await request(server).get('/1');
      expect(res.status).toBe(404);
    });
  });

  describe('GET /?param=val', () => {
    it('should return only values with the same author param', async () => {
      const data = [
        { title: 'Node JS conference', author: 'Reyner', _tags: ['nodejs', 'express'] },
        { title: 'React JS conference', author: 'Reyner', _tags: ['nodejs', 'express'] },
        { title: 'Express JS conference', author: 'Reyner', _tags: ['nodejs', 'express'] },
        { title: 'Express JS conference', author: 'Carlos', _tags: ['nodejs', 'express'] },
        { title: 'Game JS conference', author: 'Carlos', _tags: ['nodejs', 'express'] },
      ];

      await Article.insertMany(
        data,
      );

      const res = await request(server)
        .get('/?author=Reyner');

      expect(res.body.result.length).toBe(3);
    });

    it('should return only values with the two params author and title', async () => {
      const data = [
        { title: 'Node JS conference', author: 'Reyner', _tags: ['nodejs', 'express'] },
        { title: 'React JS conference', author: 'Reyner', _tags: ['nodejs', 'express'] },
        { title: 'Express JS conference', author: 'Reyner', _tags: ['nodejs', 'express'] },
        { title: 'Express JS conference', author: 'Carlos', _tags: ['nodejs', 'express'] },
        { title: 'Game JS conference', author: 'Carlos', _tags: ['nodejs', 'express'] },
      ];

      await Article.insertMany(
        data,
      );

      const res = await request(server)
        .get('/?author=Reyner&title=game');

      expect(res.body.result.length).toBe(4);
    });
  });

  describe('POST /', () => {
    it("should save the article if it's valid", async () => {
      const res = await request(server)
        .post('/')
        .send({ title: 'Node JS conference', author: 'Reyner Loza', tags: ['nodejs', 'express'] });

      const article = await Article.findOne({ author: 'Reyner Loza' });

      expect(article).not.toBeNull();
    });
  });

  describe('DELETE /:id', () => {
    it('should return 404 if a invalid id is passed as param', async () => {
      const res = await request(server).delete('/1234');
      expect(res.status).toBe(404);
    });
    it('should return 404 if the article id wasnt found to eliminate', async () => {
      // lest create a new article
      const article = new Article({ title: 'Node JS conference', author: 'Reyner Loza', _tags: ['nodejs', 'express'] });
      const article2 = new Article({ title: 'Node JS meeting', author: 'Reyner Loza', _tags: ['nodejs', 'express'] });

      // we only save the article2

      await article2.save();

      // article._id is a valid moongose id but we wont save it to test the case
      // is not found
      const res = await request(server)
        .delete(`/${article._id}`);

      expect(res.status).toBe(404);
    });
    it('should return 200 if the article id was found to eliminate', async () => {
      // lest create a new article
      const article = new Article({ title: 'Node JS conference', author: 'Reyner Loza', _tags: ['nodejs', 'express'] });

      await article.save();

      const res = await request(server)
        .delete(`/${article._id}`);

      expect(res.status).toBe(200);
    });
  });
});
