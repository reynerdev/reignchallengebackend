const express = require('express')
const mongoose = require('mongoose')
const config = require('config')
const app = express()
require('./src/swagger')(app)

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(require('./src/routes/ArticleRoutes'))

const db = mongoose.connect(config.get('db'))
  .then(db => console.log(`Db is connected to ${config.get('db')}`))
  .catch(err => console.error((err)))


const server = app.listen(config.port)

module.exports = server

