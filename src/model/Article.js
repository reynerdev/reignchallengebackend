const mongoose = require('mongoose');

const ArticleSchema = new mongoose.Schema({
  title: {
    type: String,
  },
  url: {
    type: String,
  },
  author: {
    type: String,
  },
  points: {
    type: mongoose.Schema.Types.Mixed,
  },
  story_text: {
    type: String,
  },
  comment_text: {
    type: String,
  },
  num_comments: {
    type: mongoose.Schema.Types.Mixed,
  },
  story_id: {
    type: Number,
  },
  story_title: {
    type: String,
  },
  story_url: {
    type: String,
  },
  parent_id: {
    type: Number,
  },
  created_at_i: {
    type: 'Number',
  },
  _tags: {
    type: [
      String,
    ],
  },
  objectID: {
    type: String,
  },
  _highlightResult: {
    author: {
      value: {
        type: String,
      },
      matchLevel: {
        type: String,
      },
      matchedWords: {
        type: [String],
      },
    },
    comment_text: {
      value: {
        type: String,
      },
      matchLevel: {
        type: String,
      },
      fullyHighlighted: {
        type: Boolean,
      },
      matchedWords: {
        type: [
          String,
        ],
      },
    },
    story_title: {
      value: {
        type: String,
      },
      matchLevel: {
        type: String,
      },
      matchedWords: {
        type: Array,
      },
    },
    story_url: {
      value: {
        type: String,
      },
      matchLevel: {
        type: String,
      },
      matchedWords: {
        type: [String],
      },
    },
  },
});

module.exports = mongoose.model('Article', ArticleSchema);
