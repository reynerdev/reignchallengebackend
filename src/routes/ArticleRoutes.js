const express = require('express');

const router = express.Router();

const ArticleController = require('../controller/ArticleController');

router.get('/populate', ArticleController.populate);
router.get('/', ArticleController.find);
router.get('/:id', ArticleController.findById);
router.post('/', ArticleController.createArticle);
router.delete('/:id', ArticleController.deleteById);

module.exports = router;
