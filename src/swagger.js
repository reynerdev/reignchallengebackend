const swaggerUi = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');
const path = require('path');

const swaggerDefinition = {
  openapi: '3.0.0',
  info: {
    title: 'Reign',
    version: '1.0.0',
    description: 'Api for challenge Reign',
    license: {
      name: 'MIT',
      url: 'https://choosealicense.com/licenses/mit/',
    },
  },
  servers: [
    {
      url: 'http://localhost:3000',
      description: 'Local server',
    },
  ],
};
const options = {
  swaggerDefinition,
  apis: [path.join(__dirname, './*.openapi.yml')],
};

const swaggerSpec = swaggerJSDoc(options);

module.exports = (app) => {
  app.use(
    '/swagger',
    swaggerUi.serve,
    swaggerUi.setup(swaggerSpec, {
      explorer: true,
    }),
  );
};
