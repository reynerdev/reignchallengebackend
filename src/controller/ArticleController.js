const Article = require('../model/Article');
const axios = require('axios').default;
const mongoose = require('mongoose');

const handlePagination = (totalArticles, page, maxPage) => {
  let pageNumberToSkip;
  const pageStatus = {};
  // if there is page query
  const parsePage = parseInt(page);
  if (page) {
    // limit the data after pass the max page
    if (page * maxPage >= totalArticles) {
      pageNumberToSkip = Math.max(0, Math.floor(totalArticles / maxPage) - 1);
      pageStatus.previous_page = pageNumberToSkip + 1;
    } else {
      pageStatus.next_page = parsePage + 1;
      if (parsePage !== 1) {
        pageStatus.previous_page = parsePage - 1;
      }
      pageNumberToSkip = parsePage - 1;
    }
  } else {
    // asume page=1
    pageNumber = 0;
    next_page = 2;
    previous_page = 1;
  }

  return { pageStatus, pageNumberToSkip };
};

module.exports = {
  populate: async (req, res) => {
    try {
      // clean all the documents
      Article.find().remove();

      const response = await axios.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs');

      await Article.insertMany(response.data.hits);

      res.status(200).json({ message: 'new database populated' });
    } catch (error) {
      res.status(400).json({ message: 'error populating item', error });
    }
  },
  find: async (req, res) => {
    const {
      page, author, tag, title,
    } = req.query;

    const maxPage = 5;

    // if we dont have any queries
    if (!author && !tag && !title) {
      const totalArticles = (await Article.find()).length;
      const { pageNumberToSkip, pageStatus } = handlePagination(totalArticles, page, maxPage);
      const search = await Article.find().limit(maxPage).skip(maxPage * pageNumberToSkip);
      const result = { ...pageStatus, total: search.length, result: search };
      return res.status(200).json({ message: 'retrieve data', ...result });
    }

    //  multi query search type either a author or title or tag value.
    // the result will be the join of the different query. $or
    const query = { $or: [] };

    // we use regex in order to match similarities
    if (req.query.hasOwnProperty('author')) {
      query.$or.push({ author: { $regex: author, $options: 'i' } });
    }

    if (req.query.hasOwnProperty('title')) {
      query.$or.push({ title: { $regex: title, $options: 'i' } });
    }

    // $in query for the tags
    query.$or.push({ _tags: { $in: tag } });

    try {
      const totalArticles = (await Article.find(query)).length;
      const { pageNumberToSkip, pageStatus } = handlePagination(totalArticles, page, maxPage);
      const search = await Article.find(query).limit(maxPage).skip(maxPage * pageNumberToSkip);
      const result = { ...pageStatus, total: search.length, result: search };
      return res.status(200).json({ message: 'retrieve data', ...result });
    } catch (error) {
      return res.status(400).json({ message: 'error retreiving  data', error });
    }
  },
  findById: async (req, res) => {
    const { id } = req.params;
    // check if the id is valid ObjectId in moongodb
    if (!mongoose.Types.ObjectId.isValid((id))) {
      return res.status(404).send('Incorrect ID');
    }

    const article = await Article.findOne({ _id: id });

    if (!article) {
      return res.status(404).json({ message: "article wasn't found" });
    }

    return res.status(200).json({ message: 'article was found', article });
  },
  deleteById: async (req, res) => {
    const { id } = req.params;
    // check if the id is valid ObjectId in moongodb
    if (!mongoose.Types.ObjectId.isValid((id))) {
      return res.status(404).send('Incorrect ID');
    }
    try {
      const response = await Article.findOneAndRemove({ _id: id });
      // if no article found
      if (!response) return res.status(404).json({ message: "article wasn't found with that id" });
      return res.status(200).json({ message: 'delete data success', ...response });
    } catch (error) {
      return res.status(400).json({ message: 'error deleting  data', error });
    }
  },
  createArticle: async (req, res) => {
    const newArticle = new Article({
      title: req.body.title,
      author: req.body.author,
      _tags: req.body.tags,
    });

    await newArticle.save();

    return res.status(200).json({ message: 'Article created', newArticle });
  },

};
